<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function (Blueprint $table) {
			$table->id();


			//foreign key user
			$table->foreignId('user_id');
                        $table->foreign('customer_id')->references('id')->on('customers');

			$table->string('invoice_number',20)->unique();
			$table->double('total_amount', 8, 2);
			$table->enum('status', ['new', 'processed']);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('orders');
	}
}
