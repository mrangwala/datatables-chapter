<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_items', function (Blueprint $table) {
			$table->id();

			//foreign key order_id
			$table->foreignId('order_id');
			$table->foreign('order_id')->references('id')->on('orders');

			//foreign key product_id
			$table->foreignId('product_id');
			$table->foreign('product_id')->references('id')->on('products');

			$table->float('quantity', 8, 1);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('order_items');
	}
}
