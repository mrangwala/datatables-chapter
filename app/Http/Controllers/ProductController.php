<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DataTables;
class ProductController extends Controller
{
	public function index()
	{
		return view('products');
	}

	public function getData()
	{
		$products = Product::query();
		return DataTables::eloquent($products)
			->editColumn('in_stock', function(Product $product) {
				return $product->in_stock ? 'Yes' : 'No';
			})
			->editColumn('price', function(Product $product) {
				return $product->price.' ₹';
			})
			->toJson();
	}
}
