<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Product;
use App\Order;
use App\OrderItem;
class FakeController extends Controller
{
	protected $faker;
	public function __construct()
	{
		$this->faker = \Faker\Factory::create();
	}
	public function customers()
	{
		die('This table has already been populated!');
		$customer = array();
		for($i = 0;$i<200;$i++)
		{
			$customer['name'] = $this->faker->name;
			$customer['email'] = $this->faker->unique()->safeEmail;
			Customer::create($customer);
		}
	}


	public function products()
	{
		die('This table has already been populated!');
		$product = array();
		for($i = 0; $i < 80; $i++)
		{
			$product['name'] = $this->faker->unique()->word;
			$product['price'] = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1000);
			$product['in_stock'] = $this->faker->numberBetween(0,1);
			Product::create($product);

		}
	}

	public function orders()
	{
		die('This table has already been populated!');
		$invoice_domain = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$customers = Customer::all();
		$status = array('new','processed');
		$order = array();
		foreach($customers as $customer)
		{
			$order['customer_id'] = $customer->id;
			$order_count = rand(1,5);
			for($i = 0; $i < $order_count;$i++)
			{
				$order['invoice_number'] = substr(str_shuffle($invoice_domain),0, 10);
				$order['total_amount'] = 0;
				$order['status'] = $status[rand(0,1)];
				Order::create($order);
			}
		}
	}

	public function orderItems()
	{
		die('This table has already been populated!');
		$products = Product::all();
		$orders = Order::all();
		$order_item = array();
		foreach($orders as $order)
		{
			$order_item['order_id'] = $order->id;
			$order_total = 0;
			$item_ids = $this->UniqueRandomProducts(rand(1,3));
			foreach($item_ids as $item_id)
			{
				$order_item['product_id'] = $products[$item_id]->id;
				$order_item['quantity'] = rand(1,10);


				$order_total += $products[$item_id]->price * $order_item['quantity'];

				OrderItem::create($order_item);
			}

			Order::where('id',$order->id)->update(['total_amount' => $order_total]);

		}
	}
	protected function UniqueRandomProducts( $quantity) {
		$numbers = range(0, 19);
		shuffle($numbers);
		return array_slice($numbers, 0, $quantity);
	}
}
