<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use DataTables;

class OrderController extends Controller
{
	public function index()
        {
                return view('orders');
        }

        public function getData()
        {
                $orders = Order::query();
                return DataTables::eloquent($orders)
                        ->addColumn('customer', function(Order $order) {
                                return $order->customer->name;
			})
			->addColumn('action', function(Order $order) {
				$id = $order->id;
				$route = route('order_process',[$id]);
                                return view('action',compact('route'));
                        })
                        ->editColumn('created_at', function(Order $order) {
                                return $order->created_at->format('d/m/Y');
			})
			->editColumn('total_amount', function(Order $order) {
                                return $order->total_amount.' ₹';
                        })
                        ->toJson();
	}


	public function process($id)
	{
		$order = Order::where('id',$id)->first();
		if($order)
		{

	
			$order->status = 'processed';
			$order->save();


			activity()
				->causedBy(auth()->user())
				->log(auth()->user()->name.' processed the order '.$id);

			session(['status' => __('chapter.processed')]);
			return view('order_details',compact('order'));
		}
		else
		{
			session(['error' => __('chapter.invalid')]);
			return redirect('home');
		}
	}
}
