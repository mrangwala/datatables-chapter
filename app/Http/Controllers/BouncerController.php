<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Silber\Bouncer\Bouncer;
use App\User;
class BouncerController extends Controller
{
	protected $bouncer;
	public function __construct()
	{
		$this->bouncer = Bouncer::create();
	}
	public function rolesAbilities()
	{
		$admin = User::where('email','admin@example.com')->first();

		$this->bouncer->assign('admin')->to($admin);

		$shop_manager = User::where('email','shop_manager@example.com')->first();

		$this->bouncer->assign('shop_manager')->to($shop_manager);

		$user_manager = User::where('email','user_manager@example.com')->first();

		$this->bouncer->assign('user_manager')->to($user_manager);

		$this->bouncer->allow('admin')->to('customers');
		$this->bouncer->allow('admin')->to('products');
		$this->bouncer->allow('admin')->to('orders');
		$this->bouncer->allow('shop_manager')->to('products');
		$this->bouncer->allow('shop_manager')->to('orders');
		$this->bouncer->allow('user_manager')->to('customers');
		die('Roles and abilities created and assigned');
	}
}
