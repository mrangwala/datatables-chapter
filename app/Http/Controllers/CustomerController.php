<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Customer;
use App\User;
class CustomerController extends Controller
{

	public function index()
	{
		return view('customers');
	}

	public function getData()
	{
		$customers = Customer::query();
		return DataTables::eloquent($customers)->editColumn('created_at', function(Customer $customer) {
			return $customer->created_at->format('d/m/Y');
		})->toJson();
	}
}
