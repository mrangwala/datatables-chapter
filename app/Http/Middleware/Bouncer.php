<?php

namespace App\Http\Middleware;

use Closure;

class Bouncer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $ability = $request->route()->getAction()['ability'];
	    if(auth()->user()->cannot($ability))
	    {
		    session(['error' => __('chapter.auth_error')]);
		    return redirect('home');
	    }
        return $next($request);
    }
}
