<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	//status - 'new' or 'processed'
    protected $fillable = [
                'customer_id', 'invoice_number','total_amount','status',
	];
    public function orderItems()
    {
	    return $this->hasMany('App\OrderItem');
    }

    public function customer()
    {
	    return $this->belongsTo('App\Customer');
    }
}
