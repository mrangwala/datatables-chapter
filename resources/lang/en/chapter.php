<?php

return [

	'auth_error' => 'Sorry you are unauthorized to access that page.',
	'invalid' => 'Invalid request!',
	'processed' => 'Order has been marked as processed',
	'customers' => 'Customers',
	'products' => 'Products',
	'orders' => 'Orders',
	'menu' => 'Menu',
];
