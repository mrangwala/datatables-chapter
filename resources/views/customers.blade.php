@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('chapter.customers')</div>

                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif


		    <table class="table table-bordered" id="customers-table">
		        <thead>
		            <tr>
		                <th>Name</th>
		                <th>Email</th>
		                <th>Registration Date</th>
		            </tr>
		        </thead>
		    </table>
<br><br>
			<div><a href="{{ route('home') }}">Back</a></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
	var usersTable = $('#customers-table').DataTable({
	ajax: "{{ route('customers_data') }}",
		columns: [
			{data: 'name'},
			{data: 'email' , orderable : false},
			{data: 'created_at', searchable: false , orderable : false}
		]
	});
});
</script>
@endpush
