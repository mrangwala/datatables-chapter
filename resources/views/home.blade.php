@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('chapter.menu')</div>

                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
			    {{ session('error') }}
				{{ Session::forget('error') }}
                        </div>
                    @endif
		    @if ( Auth::user()->can('customers'))
                    <a href="{{ route('customers')}}">@lang('chapter.customers')</a><br>
                    @endif
		    @if ( Auth::user()->can('products'))
		    <a href="{{ route('products')}}">@lang('chapter.products')</a><br>
		    @endif
		    @if ( Auth::user()->can('orders'))
                    <a href="{{ route('orders')}}">@lang('chapter.orders')</a><br>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
