@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('chapter.menu')</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
			    {{ session('status') }}
				{{ Session::forget('status') }}
                        </div>
			@endif
			Customer - {{ $order->customer->name}} <br>
			Invoice No. - {{ $order->invoice_number}} <br>
			Grand Total - {{ $order->total_amount}}&#8377; <br>
			Status - {{ $order->status}} <br>
			

			<table>
				<thead>
<tr>
					<th>Product</th>
                                        <th>Quantity</th>
					<th>Price</th>
					<th>Total</th>
</tr>
				</thead>
				<tbody>
				@foreach ($order->orderItems as $item)
					<tr>
					    <td>{{$item->product->name}}</td>
						<td>{{$item->quantity}}</td>
						<td>{{$item->product->price}} &#8377;</td>
						<td>{{$item->quantity * $item->product->price}} &#8377;</td>
					</tr>
@endforeach

				</tbody>
			</table>
<a href="{{ route('orders') }}">Back</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
