@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
	<div class="col-md-8">
	    <div class="card">
		<div class="card-header">@lang('chapter.orders')</div>

		<div class="card-body">
		    @if (session('error'))
			<div class="alert alert-danger" role="alert">
			    {{ session('error') }}
			</div>
		    @endif


		    <table class="table table-bordered" id="orders-table">
			<thead>
			    <tr>
				<th>Customer</th>
				<th>Invoice Id</th>
				<th>Total Amount</th>
				<th>Status</th>
				<th>Ordered On</th>
				<th>Details</th>
			    </tr>
			</thead>

		    </table>
<br><br>
			<div><a href="{{ route('home') }}">Back</a></div>

		</div>
	    </div>
	</div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function() {
	var productsTable = $('#orders-table').DataTable({
	ajax: "{{ route('orders_data') }}",
		columns: [
{data: 'customer'},
{data: 'invoice_number' , orderable : false},
{data: 'total_amount', orderable : false},
{data: 'status',orderable : false},
{data: 'created_at'},
{data: 'action',orderable : false}

]
});
});
</script>
@endpush
