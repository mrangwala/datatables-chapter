@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('chapter.products')</div>

                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif


		    <input type="radio" id="yes" name="in_stock" value="yes" class="stock-radio">
  <label for="yes">In Stock</label>
  <input type="radio" id="no" name="in_stock" value="no" class="stock-radio">
  <label for="no">Out of Stock</label>
  <input type="radio" id="both" name="in_stock" value="both" class="stock-radio" checked>
  <label for="other">Both</label>
<br><br>
		    <table class="table table-bordered" id="products-table">
		        <thead>
		            <tr>
		                <th>Name</th>
		                <th>Price</th>
		                <th>In Stock</th>
		            </tr>
			</thead>

		    </table>
<br><br>
			<div><a href="{{ route('home') }}">Back</a></div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
	    var value = $("input[name='in_stock']:checked").val();
	    var compare =  data[2];
	if(compare == 'Yes')
	{
		compare = 'yes';
	}
	else
	{
		compare = 'no';
	}

        if (value == 'both' || compare == value)
        {
            return true;
        }
        return false;
    }
);
$(document).ready(function() {
	var productsTable = $('#products-table').DataTable({
	ajax: "{{ route('products_data') }}",
		columns: [
			{data: 'name'},
			{data: 'price' , orderable : false},
			{data: 'in_stock', orderable : false}
		]
});
$('.stock-radio').click(function() {
        productsTable.draw();
    });
});
</script>
@endpush
