<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index' );

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

Route::get('faker/customers','FakeController@customers');
Route::get('faker/products','FakeController@products');
Route::get('faker/orders','FakeController@orders');
Route::get('faker/order_items','FakeController@orderItems');
Route::get('bouncer/create','BouncerController@rolesAbilities');
Route::group(['middleware' => ['auth','bouncer'] , 'ability' => 'customers'], function () {
	Route::get('customers','CustomerController@index')->name('customers');
	Route::get('customers/data','CustomerController@getData')->name('customers_data');
});


Route::group(['middleware' => ['auth','bouncer'] , 'ability' => 'products'], function () {
        Route::get('products','ProductController@index')->name('products');
        Route::get('products/data','ProductController@getData')->name('products_data');
});


Route::group(['middleware' => ['auth','bouncer'] , 'ability' => 'orders'], function () {
        Route::get('orders','OrderController@index')->name('orders');
	Route::get('orders/data','OrderController@getData')->name('orders_data');
	Route::get('orders/process/{id}','OrderController@process')->name('order_process');
});


